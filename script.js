document.addEventListener('DOMContentLoaded', () => {
    const slider = document.getElementById("customRange1");
    const sliderValue = document.getElementById("sliderValue");
    const enterpriseCard = document.getElementById("enterprise");
    const proCard = document.getElementById("pro");
    const freeCard = document.getElementById("free");
    slider.addEventListener("input", function () {
        sliderValue.textContent = slider.value;
        if (slider.value > 10 && slider.value <= 20) {
            enterpriseCard.style.boxShadow = "none";
            proCard.style.boxShadow = "5px 5px 10px 0px black";
            freeCard.style.boxShadow = "none";
        }
        if (slider.value > 20 && slider.value <= 30) {
            proCard.style.boxShadow = "none";
            enterpriseCard.style.boxShadow = "5px 5px 10px 0px black";
            freeCard.style.boxShadow = "none";
        }
        if (slider.value > 0 && slider.value <= 10) {
            proCard.style.boxShadow = "none";
            enterpriseCard.style.boxShadow = "none";
            freeCard.style.boxShadow = "5px 5px 10px 0px black";
        }
    });
    function resetSlider() {
        slider.value = 0;
        sliderValue.textContent = "0";
    }
    window.addEventListener("load", resetSlider);


    function isScrollAtEnd() {
        const scrollHeight = document.documentElement.scrollHeight;
        const scrollTop = Math.ceil(window.scrollY)
        const windowHeight = window.innerHeight;
        return scrollTop >= scrollHeight - windowHeight;
    }

    window.addEventListener('scroll', function () {
        if (isScrollAtEnd()) {
            this.document.getElementById("loader").style.display = 'block';
            this.fetch("https://random-data-api.com/api/users/random_user?size=20").then((response) => {
                return response.json();
            }).then((data) => {
                console.log(data);
                this.document.getElementById("loader").style.display = 'none';
                data.forEach(ele => {
                    let targetDiv = this.document.getElementById("lazyLoadingDiv");
                    const newElement = this.document.createElement('h4');
                    newElement.textContent = ele["first_name"];
                    targetDiv.appendChild(newElement);
                });
            })
        }
    });
});
